<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0"
                xmlns:tns="http://www.dei.isep.ipp.pt/lprog">
    <xsl:output method="html"/>
    
    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; margin-left:20%; margin-right:20%;">
                <!--Capa-->
                <xsl:apply-templates select="tns:relat�rio/tns:p�ginaRosto"/>
            
                <!--�ndice e introdu��o-->
                <xsl:apply-templates select="tns:relat�rio/tns:corpo/tns:introdu��o"/>

                <!--An�lise, A linguagem, Transforma��es-->
                <xsl:apply-templates select="tns:relat�rio/tns:corpo/tns:sec��es"/>

                <!--Conclus�o-->
                <xsl:apply-templates select="tns:relat�rio/tns:corpo/tns:conclus�o"/>

                <!--Refer�ncias-->
                <xsl:apply-templates select="tns:relat�rio/tns:corpo/tns:refer�ncias"/>

                <!--Anexos-->
                <xsl:apply-templates select="tns:relat�rio/tns:anexos"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="tns:relat�rio/tns:p�ginaRosto">
        <center>
            <img src="{tns:logotipoDEI}"></img>
            <br></br>
            <br></br>
            <h1>
                <p>
                    <xsl:value-of select="tns:tema"/>
                </p>
                <p>Licenciatura Engenharia Inform�tica</p>
                <p>
                    <xsl:value-of select="tns:disciplina/tns:sigla"/> - <xsl:value-of select="tns:disciplina/tns:designa��o"/> -  <xsl:value-of select="tns:disciplina/tns:anoCurricular"/>� ano</p>
            </h1>
            <br></br>
            <p>
                <font size="4">
                    <i>Autores: </i>
                    <b>
                        <xsl:apply-templates select="tns:autor"/>
                    </b>
                </font>
            </p>
            <p>
                <font size="4">
                    <i>Turma: </i>
                    <b>
                        <xsl:value-of select="tns:turmaPL"/>
                    </b>
                </font>
            </p>
            <p>
                <font size="4">
                    <i>Docente: </i>
                    <b>
                        <xsl:value-of select="tns:profPL"/>
                    </b>
                </font>
            </p>
            <br></br>
            <h1>
                <xsl:value-of select="/tns:relat�rio/tns:p�ginaRosto/tns:data"/>
            </h1>
        </center>
        <br></br>
        <br></br>
    </xsl:template>

    <xsl:template match="tns:relat�rio/tns:p�ginaRosto/tns:autor">
        <br></br>
        <xsl:value-of select="tns:nome"/> - <xsl:value-of select="tns:n�mero"/> - <xsl:value-of select="tns:mail"/>
    </xsl:template>

    <xsl:template match="tns:relat�rio/tns:corpo/tns:introdu��o">
        <xsl:apply-templates select="tns:bloco"/>
    </xsl:template>

    <xsl:template match="tns:bloco">
        <xsl:apply-templates select="node()"/>
        <br></br>
        <br></br>
    </xsl:template>

    <xsl:template match="tns:paragr�fo[position()>1]">
        <p  style="text-indent:35px;">
            <xsl:value-of select="."/>
        </p>
    </xsl:template>

    <!--T�tulos dos t�picos do relat�rio-->
    <xsl:template match="tns:paragr�fo[position()=1]">
        <a name="{tns:bold}">
            <h1 style="color: #144290">
                <xsl:apply-templates select="tns:bold"/>
                <hr style="color: #48B4EF;"></hr>
            </h1>
        </a>
    </xsl:template>

    <xsl:template match="tns:bold">
        <b>
            <xsl:value-of select="."/>
        </b>
    </xsl:template>

    <xsl:template match="tns:it�lico">
        <i>
            <xsl:value-of select="."/>
        </i>
    </xsl:template>
    
    <xsl:template match="tns:sublinhado">
        <u>
            <xsl:value-of select="."/>
        </u>
    </xsl:template>
    
    <xsl:template match="tns:cita��o">
        <i>
            "<xsl:value-of select="."/>"
        </i>
    </xsl:template>
    
    <xsl:template match="tns:relat�rio/tns:corpo/tns:sec��es">
        <xsl:apply-templates select="tns:an�lise"/>
        <xsl:apply-templates select="tns:linguagem"/>
        <xsl:apply-templates select="tns:transforma��es"/>
    </xsl:template>
    
    <xsl:template match="tns:an�lise">
        <xsl:apply-templates select="tns:bloco"/>
    </xsl:template>

    <xsl:template match="tns:linguagem">
        <xsl:apply-templates select="tns:bloco"/>
    </xsl:template>

    <xsl:template match="tns:transforma��es">
        <xsl:apply-templates select="tns:bloco"/>
    </xsl:template>
    
    <xsl:template match="tns:relat�rio/tns:corpo/tns:conclus�o">
        <xsl:apply-templates select="tns:bloco"/>
    </xsl:template>
    
    <xsl:template match="tns:relat�rio/tns:corpo/tns:refer�ncias">
        <a name="Refer�ncias">
            <h1 style="color: #144290">
                <b>Refer�ncias</b>
                <hr style="color: #48B4EF;"></hr>
            </h1>
        </a>
        <table cellpadding="10">
            <xsl:apply-templates select="tns:refWeb"/>
        </table>
        <table cellpadding="10">
            <xsl:apply-templates select="tns:refBibliogr�fica"/>
        </table>
        <br></br>
    </xsl:template>
    
    <xsl:template match="tns:refWeb">
        <xsl:for-each select=".">
            <tr>
                <td>
                    <xsl:apply-templates select="tns:URL"/>
                </td>
                <td>
                    <xsl:apply-templates select="tns:descri��o"/>
                </td>
                <td>
                    <xsl:apply-templates select="tns:consultadoEm"/>
                </td>
            </tr>
            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="tns:URL">
        <a href="{.}">
            <xsl:value-of select="."/>
        </a>
    </xsl:template>
    
    <xsl:template match="tns:descri��o">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="tns:consultadoEm">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="tns:refBibliogr�fica">
        <xsl:for-each select=".">
            <tr>
                <td>
                    <i>
                        <xsl:apply-templates select="tns:t�tulo"/>
                    </i>
                </td>
                <td>
                    <xsl:apply-templates select="tns:dataPublica��o"/>
                </td>
                <td>
                    <xsl:apply-templates select="tns:autor"/>
                </td>
            </tr>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="tns:t�tulo">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="tns:dataPublica��o">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="tns:autor">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="tns:relat�rio/tns:anexos">
        <xsl:apply-templates select="tns:bloco"/>
    </xsl:template>

    <xsl:template match="tns:figura">
        <center>
        <img src="{@src}" border="2"></img>
        <br>
            <i>
                <xsl:value-of select="@descri��o"/>
            </i>
        </br>
        <br></br>
        <br></br>
        </center>
    </xsl:template>
    
    <xsl:template match="tns:listaItems/tns:item">
        <ul>
            <p>
                <li>
                    <xsl:value-of select="."/>
                </li>
            </p>
        </ul>
    </xsl:template>

    <!--Coloca ancoras nos t�tulos para serem acessiveis a partir do �ndice-->
    <xsl:template match="tns:relat�rio/tns:corpo/tns:introdu��o/tns:bloco[position()=1]/tns:listaItems/tns:item">
        <ul>
            <p>
                <li>
                    <a href="#{.}" style="color:blue;">
                        <xsl:value-of select="."/>
                    </a>
                </li>
            </p>
        </ul>
    </xsl:template>
</xsl:stylesheet>