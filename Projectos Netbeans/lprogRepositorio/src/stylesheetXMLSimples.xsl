<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0"
                xmlns:tns="http://xml.dei.isep.ipp.pt/schema/Repositorio">
    <xsl:output method="xml"/>

    <xsl:template match="/">
        <xsl:element name="repositorio">
            <xsl:element name="instituicao">
                <xsl:value-of select="tns:repositorio/@instituicao"/>
            </xsl:element>
            <xsl:apply-templates select="tns:repositorio/tns:pacote"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:repositorio/tns:pacote">
        <xsl:element name="pacote">
            <xsl:apply-templates select="@nome"/>
            <xsl:apply-templates select="@ref"/>
            <xsl:apply-templates select="tns:aplicacao"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="@nome">
        <xsl:element name="nomePacote">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@ref">
        <xsl:element name="ref">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:aplicacao">
        <xsl:element name="aplicacao">
            <xsl:apply-templates select="tns:descricao/tns:nome"/>
            <xsl:apply-templates select="tns:descricao/tns:icon"/>
            <xsl:apply-templates select="tns:descricao/tns:website"/>
            <xsl:apply-templates select="tns:descricao/tns:so"/>
            <xsl:apply-templates select="tns:descricao/tns:linguagem"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:descricao/tns:nome">
        <xsl:attribute name="nomeAplicacao">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="tns:descricao/tns:icon">
        <xsl:attribute name="icon">
            <xsl:value-of select="@icon"/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="tns:descricao/tns:website">
        <xsl:attribute name="website">
            <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    
    <xsl:template match="tns:descricao/tns:so">
        <xsl:element name="so">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:descricao/tns:linguagem">
        <xsl:element name="linguagem">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
</xsl:stylesheet>
