<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0"
                xmlns:tns="http://xml.dei.isep.ipp.pt/schema/Repositorio">
    <xsl:output method="html"/>

    <xsl:variable name="rodape">
                Linguagens e Programa��o 2013/2014 - Vasco Ribeiro e Telmo Moreira
    </xsl:variable>
    
    <xsl:template match="/">
        <html>
            <head>
                <title>Reposit�rio</title>
            </head>
            <body style="background-image:url('outrasimgs/bg.jpg'); background-repeat:no-repeat">
                <p>
                    <h3 align="right" style="color: #ffffff; font-family: Helvetica; font-size:30px; position:relative; top: 30px;">Reposit�rio de Software do <xsl:value-of select="tns:repositorio/@instituicao"/></h3>
                </p>
                <p align="right" style="color: black; font-family: Helvetica; font-size:10px;">
                    <br></br>
                    <a href="psimples.html">Simples</a>
                    <br></br>
                    <a href="pavancado.html">Avan�ado</a>
                    <br></br>
                    <a href="pgrelha.html">Grelha</a>
                </p>
                <p align="center" style="color: black; font-family: Helvetica; font-size:35px;">
                    Estat�sticas
                </p>
                <p align="center" style="color: black; font-family: Helvetica; font-size:20px;">
                    <table cellpadding="5" style="font-family: Helvetica; border:0px solid #144290; border-collapse:collapse;">
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">
                                <i>Total Pacotes de Instala��o:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote)"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">  
                                <i>Total Aplica��es:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao)"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">    
                                <i>Total Aplica��es para Investiga��o:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(//tns:objectivo[.='Investiga��o'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <i>Total Aplica��es para Educa��o:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(//tns:objectivo[.='Educa��o'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;"> 
                                <i>Total Aplica��es para Alunos:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(//tns:nivelAcesso[.='Aluno'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">  
                                <i>Total Aplica��es para Docentes:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(//tns:nivelAcesso[.='Docente'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <i>Total Aplica��es para Investigadores:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(//tns:nivelAcesso[.='Investigador'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <i>Total Licen�as:</i>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">
                                <i>Freeware:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:licenca[.='Freeware'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">  
                                <i>Trialware:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:licenca[.='Trialware'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">    
                                <i>Donationware:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:licenca[.='Donationware'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <i>Adware:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:licenca[.='Adware'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;"> 
                                <i>Open-source:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:licenca[.='Open-source'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">  
                                <i>Comercial:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:licenca[.='Comercial'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <i>Outras:</i>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:licenca[.='Outra'])"/>
                            </td>
                        </tr>
                    </table>
                    <br></br>
                    <br>
                        <font style="font-size:30px;">Linguagens</font>
                    </br>
                    <br></br>
                    <table cellpadding="5" style="font-family: Helvetica; border:0px solid #144290; border-collapse:collapse;">
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">
                                <img width="60" height="60" src="linguagem/64pt.png" title="Portugu�s"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Portugu�s'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">  
                                <img width="60" height="60" src="linguagem/64uk.png" title="Ingl�s"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Ingl�s'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">    
                                <img width="60" height="60" src="linguagem/64ru.png" title="Russo"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Russo'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <img width="60" height="60" src="linguagem/64de.png" title="Alem�o"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Alem�o'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;"> 
                                <img width="60" height="60" src="linguagem/64jp.png" title="Japon�s"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Japon�s'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">  
                                <img width="60" height="60" src="linguagem/64es.png" title="Espanhol"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Espanhol'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <img width="60" height="60" src="linguagem/64fr.png" title="Franc�s"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Franc�s'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <img width="60" height="60" src="linguagem/64ch.png" title="Chin�s"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Chin�s'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <img width="60" height="60" src="linguagem/64it.png" title="Italiano"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Italiano'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <img width="60" height="60" src="linguagem/64pl.png" title="Polaco"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:linguagem[.='Polaco'])"/>
                            </td>
                        </tr>
                    </table>
                    <br></br>
                    <br>
                        <font style="font-size:30px;">Sistemas Operativos</font>
                    </br>
                    <br></br>
                    <table cellpadding="5" style="font-family: Helvetica; border:0px solid #144290; border-collapse:collapse;">
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">
                                <img width="60" height="60" src="so/windows.png" title="Windows"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:so[.='Windows'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">  
                                <img width="60" height="60" src="so/linux.png" title="Linux"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:so[.='Linux'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">    
                                <img width="60" height="60" src="so/mac.png" title="Mac OS"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:so[.='Mac OS'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <img width="60" height="60" src="so/Solaris.png" title="Solaris"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:so[.='Solaris'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;"> 
                                <img width="60" height="60" src="so/android.png" title="Android"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:so[.='Android'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">
                                <img width="60" height="60" src="so/ios.jpg" title="iOS"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:so[.='iOS'])"/>
                            </td>
                        </tr>
                        <tr  style="border-bottom:1px solid #144290;">
                            <td align="right" style="font-size:20px;">   
                                <img width="60" height="60" src="so/wphone.jpg" title="Windows Phone"/>
                            </td>
                            <td align="center" style="font-size:30px;">
                                <xsl:value-of select="count(tns:repositorio/tns:pacote/tns:aplicacao/tns:descricao/tns:so[.='Windows Phone'])"/>
                            </td>
                        </tr>
                    </table>
                </p>
                <p align="right" style="color: black; font-family: Helvetica; font-size:10px;">
                    <i>
                        <br><xsl:value-of select="$rodape"/></br>
                    </i>
                </p>
            </body>
        </html>
    </xsl:template>   
</xsl:stylesheet>
