<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0"
                xmlns:tns="http://xml.dei.isep.ipp.pt/schema/Repositorio">
    <xsl:output method="html"/>

    <xsl:variable name="rodape">
                Linguagens e Programação 2013/2014 - Vasco Ribeiro e Telmo Moreira
    </xsl:variable>
    
    <xsl:template match="/">
        <html>
            <head>
                <title>Repositório</title>
            </head>
            <body style="background-image:url('outrasimgs/bg.jpg'); background-repeat:no-repeat">
                <p>
                    <h3 align="right" style="color: #ffffff; font-family: Helvetica; font-size:30px; position:relative; top: 30px;">Repositório de Software do <xsl:value-of select="tns:repositorio/@instituicao"/></h3>
                </p>
                <p align="right" style="color: black; font-family: Helvetica; font-size:10px;">
                    <br></br>
                    <a href="psimples.html">Simples</a>
                    <br></br>
                    <a href="pavancado.html">Avançado</a>
                    <br></br>
                    <a href="pestatisticas.html">Estatísticas</a>
                </p>
                <xsl:apply-templates select="tns:repositorio"/>
                <p align="right" style="color: black; font-family: Helvetica; font-size:10px;">
                    <i>
                        <br><xsl:value-of select="$rodape"/></br>
                    </i>
                </p>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="tns:repositorio">
        <table align="center" cellpadding="20" style="font-family: Helvetica; font-size: 20px; border:0px solid #144290; border-collapse:collapse;">
            <xsl:for-each select="tns:pacote/tns:aplicacao/tns:descricao">
                <xsl:sort select="tns:nome"/>
                <td align="center">
                    <xsl:apply-templates select="tns:icon"/>
                    <p>
                        <xsl:value-of select="tns:nome"/>
                    </p>
                </td>
                <!--A cada 5 aplicações cria uma nova linha-->
                <xsl:if test="(position() mod 5 = 0)">
                    <tr></tr>
                </xsl:if>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="tns:website">
        <a href="{.}">
            <xsl:apply-templates select="../tns:icon"/>
        </a>
    </xsl:template>
    
    <xsl:template match="tns:icon">
        <a href="{../tns:website}">
            <img width="50" height="50" >
                <xsl:attribute name="src">
                    <xsl:value-of select='@icon' />
                </xsl:attribute>
                <xsl:attribute name="title">
                    <xsl:apply-templates select="../../tns:nivelAcesso"/>
                </xsl:attribute>
            </img>
        </a>
    </xsl:template>
    
    <xsl:template match="tns:nivelAcesso">
        <xsl:value-of select="."/>.&#10;</xsl:template>
    
</xsl:stylesheet>
