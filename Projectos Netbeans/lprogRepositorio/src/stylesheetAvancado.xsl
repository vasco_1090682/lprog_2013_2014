<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0"
                xmlns:tns="http://xml.dei.isep.ipp.pt/schema/Repositorio">
    <xsl:output method="html"/>
    
    <xsl:variable name="rodape">
                Linguagens e Programa��o 2013/2014 - Vasco Ribeiro e Telmo Moreira
    </xsl:variable>
    
    <xsl:template match="/">
        <html>
            <head>
                <title>Reposit�rio</title>
            </head>
            <body style="background-image:url('outrasimgs/bg.jpg'); background-repeat:no-repeat">
                <p>
                    <h3 align="right" style="color: #ffffff; font-family: Helvetica; font-size:30px; position:relative; top: 30px;">Reposit�rio de Software do <xsl:value-of select="tns:repositorio/@instituicao"/></h3>
                </p>
                <p align="right" style="color: black; font-family: Helvetica; font-size:10px;">
                    <br></br>
                    <a href="psimples.html">Simples</a>
                    <br></br>
                    <a href="pgrelha.html">Grelha</a>
                    <br></br>
                    <a href="pestatisticas.html">Estat�sticas</a>
                </p>
                <xsl:apply-templates select="tns:repositorio/tns:pacote"/>
                <p align="right" style="color: black; font-family: Helvetica; font-size:10px;">
                    <br><i><xsl:value-of select="$rodape"/></i></br>
                </p>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="tns:pacote">
        <p style="color: black; font-family: Helvetica; font-size:10px; margin-top: 30px; margin-bottom: -35px; margin-left: 2px; margin-right: 0px;">Pacote de instala��o</p>
        <p style="color: black; font-family: Helvetica; font-size:30px;">
            <i>
                <xsl:value-of select="@nome"/>
            </i> 
            <font size="3px">(<xsl:value-of select="@ref"/>)</font>
        </p>
        <table cellpadding="5" style="font-family: Helvetica; border:0px solid #144290; border-collapse:collapse; position:relative; left: 15px;">
            <tr style="color: #ffffff; background: #144290;">
                <th align="center"></th>
                <th align="center"></th>
                <th align="center">Nome</th>
                <th align="center">Editor</th>
                <th align="center">Objectivo</th>
                <th align="center">G�nero</th>
                <th align="center">Licen�a</th>
                <th align="center">Vers�o</th>
                <th align="center">S.O.</th>
                <th align="center">Linguagem</th>
                <th align="center">Website</th>
                <th align="center">N�vel<br>Acesso</br></th>
                <th align="center">Chave<br>Instala��o</br></th>
            </tr>
            <xsl:for-each select="tns:aplicacao">
                <xsl:sort select="tns:descricao/tns:nome"/>
                <tr  style="border-bottom:1px solid #144290; font-size:13px;">
                    <td align="center">   
                        <xsl:value-of select="position()"/>
                    </td>
                    <td align="center">
                        <xsl:apply-templates select="tns:descricao/tns:icon"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="tns:descricao/tns:nome"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="tns:descricao/tns:editor"/>
                    </td>
                    <td align="center">
                        <xsl:apply-templates select="tns:descricao/tns:objectivo"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="tns:descricao/tns:genero"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="tns:descricao/tns:licenca"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="tns:descricao/tns:versao/tns:sequencia"/> 
                        <br></br>(<xsl:value-of select="tns:descricao/tns:versao/tns:dataLancamento"/>)
                    </td>
                    <td align="center">
                        <xsl:apply-templates select="tns:descricao/tns:so"/>
                    </td>
                    <td align="center">
                        <xsl:apply-templates select="tns:descricao/tns:linguagem"/>
                    </td>
                    <td align="center">
                        <a href="{tns:descricao/tns:website}">
                            <img src="outrasimgs/hyperlink.png" title="{tns:descricao/tns:website}" width="25" height="25"/>
                        </a>
                    </td>
                    <td align="center">
                        <xsl:apply-templates select="tns:nivelAcesso"/>
                    </td>
                    <td align="center">
                        <xsl:value-of select="tns:chaveInstalacao"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>
    
    <xsl:template match="tns:so">
        <xsl:choose>
            <xsl:when test=".='Windows'">
                <img src="so/windows.png" title="Windows" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Linux'">
                <img src="so/linux.png" title="Linux" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Mac OS'">
                <img src="so/mac.png" title="Mac OS" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Solaris'">
                <img src="so/solaris.png" title="Solaris" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Android'">
                <img src="so/android.png" title="Android" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='iOS'">
                <img src="so/ios.jpg" title="iOS" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Windows Phone'">
                <img src="so/wphone.jpg" title="Windows Phone" width="25" height="25"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tns:linguagem">
        <xsl:choose>
            <xsl:when test=".='Ingl�s'">
                <img src="linguagem/uk.png" title="Ingl�s" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Portugu�s'">
                <img src="linguagem/pt.png" title="Portugu�s" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Espanhol'">
                <img src="linguagem/es.png" title="Espanhol" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Alem�o'">
                <img src="linguagem/de.png" title="Alem�o" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Franc�s'">
                <img src="linguagem/fr.png" title="Franc�s" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Italiano'">
                <img src="linguagem/it.png" title="Italiano" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Russo'">
                <img src="linguagem/ru.png" title="Russo" width="30" height="25"/>
            </xsl:when>
            <xsl:when test=".='Polaco'">
                <img src="linguagem/pl.png" title="Polaco" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Japon�s'">
                <img src="linguagem/jp.png" title="Japon�s" width="25" height="25"/>
            </xsl:when>
            <xsl:when test=".='Chin�s'">
                <img src="linguagem/ch.png" title="Chin�s" width="25" height="25"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
     
    <xsl:template match="tns:icon">
        <img width="25" height="25" >
            <xsl:attribute name="src">
                <xsl:value-of select='@icon' />
            </xsl:attribute>
        </img>
    </xsl:template>
    
    <xsl:template match="tns:objectivo">
        <xsl:value-of select="."/>
        <br></br>
    </xsl:template>
    
    <xsl:template match="tns:nivelAcesso">
        <xsl:value-of select="." />
        <br></br>
    </xsl:template>
</xsl:stylesheet>
