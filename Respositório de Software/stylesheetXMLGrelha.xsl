<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
                version="1.0"
                xmlns:tns="http://xml.dei.isep.ipp.pt/schema/Repositorio">
    <xsl:output method="xml"/>

    <xsl:template match="/">
        <xsl:element name="repositorio">
            <xsl:element name="instituicao">
                <xsl:value-of select="tns:repositorio/@instituicao"/>
            </xsl:element>
            <xsl:apply-templates select="tns:repositorio/tns:pacote/tns:aplicacao"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:repositorio/tns:pacote/tns:aplicacao">
        <xsl:element name="aplicacao">
            <xsl:attribute name="nome">
                <xsl:value-of select="tns:descricao/tns:nome"/>
            </xsl:attribute>
            <xsl:apply-templates select="tns:descricao/tns:icon/@icon"/>
            <xsl:apply-templates select="tns:descricao/tns:website"/>
            <xsl:apply-templates select="tns:nivelAcesso"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:descricao/tns:icon/@icon">
        <xsl:element name="icon">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:descricao/tns:website">
        <xsl:element name="website">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tns:nivelAcesso">
        <xsl:element name="nivelAcesso">
            <xsl:value-of select="."/>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
